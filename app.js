var express = require('express');
app = express();

fs = require('fs')

app.get('/', function (req, res) {
  res.send('Hello World!\n');
});

app.get('/127.0.0.1', function (req, res) {
  res.send('127.0.0.1\n');
});


fs.readFile('/etc/hosts', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  console.log(data);
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
